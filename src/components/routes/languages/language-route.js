import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import { Pages } from "components/pages";

export default function LanguagesRoute() {

    const { path } = useRouteMatch();
    
    return (
        <>
            <Switch>
                <Route path={`${path}/create`} exact>
                    <Pages.LanguageCreatePage />
                </Route>
                <Route path={`${path}`} exact>
                    <Pages.LanguagesPage />
                </Route>
                <Route path={`${path}/:id`}>
                    { ( { match, history } ) => <Pages.LanguageEditPage match={ match } history={ history }/> }
                </Route>
            </Switch>
        </>
    );

}