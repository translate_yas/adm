import React, { useState } from 'react';
import { Form, Input, Divider, Select, Typography, Space } from 'antd';

import { PlusOutlined } from '@ant-design/icons'


const { Option } = Select;

const TranslateGroupSelectField = ( { translationsGroup } ) => {

    let index   = 0;
    let groups   = [];

    translationsGroup.map( group => {
            
        groups.push( group.label );

    });

    const [items, setItems] = useState( groups );
    const [name, setName] = useState('');

    const onNameChange = event => {
        setName( event.target.value );
    };

    const addItem = e => {
        e.preventDefault();
        setItems([...items, name || `New_${index++}`]);
        setName('');
    };

    return (
        <Form.Item 
            name="tr_group" 
            label="Group" 
            // rules={[{ required: true }]}
            rules={[
                { required: true, message: 'Please input Group' }
            ]}
        >
            <Select
                style={{ width: 300 }}
                placeholder="Select Group"
                dropdownRender={ menu => (
                    <>
                        { menu }
                        <Divider style={{ margin: '8px 0' }} />
                        <Space align="center" style={{ padding: '0 8px 4px' }}>
                            <Input placeholder="Please enter Type" value={name} onChange={onNameChange} />
                            <Typography.Link onClick={addItem} style={{ whiteSpace: 'nowrap' }}>
                            <PlusOutlined /> Add Group
                            </Typography.Link>
                        </Space>
                    </>
                )}
                >
                { items.map( item => (
                    <Option key={ item }>{ item }</Option>
                )) }
            </Select>
        </Form.Item>
    );
}

export default TranslateGroupSelectField;