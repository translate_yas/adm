import React from "react";
import { Table } from 'antd';

import { tableHelper } from "utils";


const TableRender = ({
    data,
    loading,
    pagination,
    setOrderBy,
    defaultOrder,
    tableBuilder
}) => {

    const { dataSource, columns } = tableBuilder({
        data : !loading && !pagination.total ? [""] : data,
        loading,
        rowAmount : tableHelper.tableRowsCount( pagination ),
        onlyHeader: !loading && !data.length
    });

    return(
        <Table 
            dataSource={ dataSource }
            columns={ columns }  
            pagination={ pagination }
            onChange={ ( pagination, filters, { column, order } ) => {
               
                if( column && order ){
                    
                    setOrderBy([
                        { 
                            column: column.columnIndex, 
                            order: order === "ascend" ? "ASC" : "DESC"  
                        }
                    ])

                } else { setOrderBy( [ defaultOrder ] ) }

            }}
            // tableLayout='fixed'
            // scroll={ { x:true } }
        />
    )

}

export default TableRender;