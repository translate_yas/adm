const fileNamefromUrl = ( url ) => {

    const { pathname }= new URL( url );
  
    return pathname.substring( pathname.lastIndexOf('/')+1 );
  
}

export default fileNamefromUrl;