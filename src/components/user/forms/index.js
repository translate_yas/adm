import UserEditForm           from "./user-edit-form";
import UserCreateForm         from "./user-create-form";

const UserForms = {
    Edit   : UserEditForm,
    Create : UserCreateForm,
};

export default UserForms;