import {
    appParamsVar,
    appParamsInitial,
    guestParamsVar,
    guestParamsInitial
} from "graphql/cache";

import { makeVarParam } from "utils";


const useVarParam = (type) => {

    if(type === "guest"){
        return makeVarParam(guestParamsVar, guestParamsInitial);
    }

    return makeVarParam(appParamsVar, appParamsInitial);

};

export default useVarParam;