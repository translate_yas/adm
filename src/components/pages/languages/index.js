import LanguagesPage from "./languages-page";
import LanguageEditPage from "./language-edit-page";
import LanguageCreatePage from "./language-create-page";

export {
    LanguagesPage,
    LanguageCreatePage,
    LanguageEditPage
}