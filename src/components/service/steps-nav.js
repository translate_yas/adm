import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";

import Icons from "components/icons";


const doStep = ({ varParams, startStep = 1}) => {

    let registration = varParams.get("registration");

    const goStep = (step) => {

        varParams.set({
            registration: {
                ...varParams.get("registration"), step
            }
        })
    }

    const prevStep = () => {
        goStep(registration.step - 1);
    }

    const nextStep = () => {
        goStep(registration.step + 1);
    }

    if(registration.step === undefined){
        goStep(startStep);
    }

    return {
        nextStep, prevStep, goStep
    }
}

const Nav = ({ stepAmount, varParams }) => {

    const history = useHistory();
    const registration = varParams.get("registration");

    const { prevStep } = doStep({ varParams });


    return(
        <div className="steps-nav-wrap">
            <div className="steps-nav-back">
                <button
                    onClick={
                        () => registration.step === 1 ? history.go(-1) : prevStep()
                    }
                >
                    <Icons.Arrow />
                </button>
            </div>

            <ul className="steps-nav">
                { [...Array(stepAmount)].map( ( x, i ) => {
                    let stepClass = "";

                    if(registration.step === i + 1 ){
                        stepClass = "active";
                    } else if( i + 1 < registration.step ) {
                        stepClass = "finished"
                    }

                    return (
                        <li key={ i } className={ `steps-nav-step ${ stepClass }` }>
                            { i + 1 }
                        </li>
                    );
                })}
            </ul>
        </div>
    );
}

const StepsNav = ({ varParams, inMainHeader = false, ...props }) => {

    useEffect(() => {

        varParams.set({
            headerClass: inMainHeader ? ["logo-off"] : [],
            leftHeaderControl: inMainHeader ?
                <Nav
                    stepAmount={ props.children.length }
                    varParams={ varParams }
                /> : false
        })

        return () => {
            varParams.set({
                headerClass: [],
                leftHeaderControl: false
            })
        }
    })

    let step = varParams.get("registration").step;
    const { nextStep } = doStep({ varParams });

    return (
        <>
            { !inMainHeader &&
                <Nav
                    stepAmount={ props.children.length }
                    varParams={ varParams }
                />
            }
            { step && React.cloneElement(
                props.children[step - 1], {
                    submitAction: nextStep
                }
            )}
        </>
    );

};

export default StepsNav;