import TranslateImportForm from "./translate-import-forms";
import TranslateCreateForm from "./translate-create-form";

export {
    TranslateImportForm,
    TranslateCreateForm
}