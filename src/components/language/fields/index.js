import LanguageEditLinkField from "./language-edit-link-field";
import LanguageDeleteButtonField from "./language-delete-button-field";

export { 
    LanguageEditLinkField,
    LanguageDeleteButtonField
}