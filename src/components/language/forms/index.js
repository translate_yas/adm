import LanguageEditForm from "./language-edit-form";
import LanguageCreateForm from "./language-create-form";

export { 
    LanguageCreateForm,
    LanguageEditForm
}