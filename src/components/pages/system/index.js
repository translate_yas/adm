import SystemInfoPage from "./system-information-page";
import SystemTestPage from "./system-test-page";

export {
    SystemInfoPage,
    SystemTestPage
}