import TranslatesGenerateJsonPage from "./translates-generate-json-page";
import TranslatesPage from "./translates-page";
import TranslateImportPage from "./translate-import-page";

export {
    TranslatesPage,
    TranslateImportPage,
    TranslatesGenerateJsonPage
}