import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import { Pages } from "components/pages";

export default function SystemPage() {

    const { path } = useRouteMatch();
    
    return (
        <>
            <Switch>
                <Route path={`${path}/information`} exact>
                    <Pages.SystemInfoPage />
                </Route>
                <Route path={`${path}/test`} exact>
                    <Pages.SystemTestPage />
                </Route>
            </Switch>
        </>
    );

}