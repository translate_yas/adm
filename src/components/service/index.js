import ScrollToTop from "./scrollToTop";
import UploadFile from "./upload-file";
import ImageLoader from "./image-loader";
import MoreText from "./more-text";
import FloatLabel from "./float-label";
import StepsNav from "./steps-nav";
import ModalStandard from "./modal-standard";
import DrawerStandart from "./drawer-standart";


export {
    ScrollToTop,
    UploadFile,
    ImageLoader,
    MoreText,
    FloatLabel,
    StepsNav,
    ModalStandard,
    DrawerStandart
}